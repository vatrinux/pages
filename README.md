## Olá, Juliano aqui ✌️🤓

##### Um humilde apaixonado por tecnologia da informação, iniciante nesse novo/velho mundo da programação e desenvolvimento na web. Minha intenção é aprender cada vez mais sobre linguagens de marcação e linguagens de programação. Espero um dia poder trabalhar com essas tecnologias !!

### Linguagens:
<div>
<a href="#"><img src="https://img.shields.io/badge/HTML5-E34F26?style=for-the-badge&logo=html5&logoColor=white"></a>
<a href="#"><img src="https://img.shields.io/badge/CSS3-1572B6?style=for-the-badge&logo=css3&logoColor=white"></a>
<a href="#"><img src="https://img.shields.io/badge/JavaScript-F7DF1E?style=for-the-badge&logo=javascript&logoColor=black"></a>
<!-- <a href="#"><img src="https://img.shields.io/badge/TypeScript-007ACC?style=for-the-badge&logo=typescript&logoColor=white"></a> -->
</div>

### Contato:
<div> 
<a href="https://vatrinux.gitlab.io/pages" target="_blank"><img src="https://img.shields.io/badge/GitLab-330F63?style=for-the-badge&logo=gitlab&logoColor=white"></a>
<a href="https://t.me/julianovatre" target="_blank"><img src="https://img.shields.io/badge/Telegram-2CA5E0?style=for-the-badge&logo=telegram&logoColor=white"></a>
<a href="https://discord.com/user/vatrinux#9556" target="_blank"><img src="https://img.shields.io/badge/Discord-7289DA?style=for-the-badge&logo=discord&logoColor=white"></a>
</div>